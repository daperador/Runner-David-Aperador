using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterController : MonoBehaviour
{
    [SerializeField]
    private Transform Bullet;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Shoot());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "PichoPinchito")
            Destroy(col.gameObject);
    }
    IEnumerator Shoot()
    {
        while (true)
        {
            float shootTime = Random.Range(0.5f, 2);
            yield return new WaitForSeconds(shootTime);
            Transform shoot = Instantiate(Bullet, transform.position, Quaternion.identity);
        }
    }
}
