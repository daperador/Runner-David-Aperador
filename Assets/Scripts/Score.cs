using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField]
    private CharacterController cc;
    private TMPro.TextMeshProUGUI m_score;
    private int scorego = 0;
    public int m_socrego
    {
        get { return scorego; }
        private set { scorego = value; }
    }
    // Start is called before the first frame update
    void Start()
    {
        cc.ScorePlayerScreen += SSPlayer;
        m_score = GetComponent<TMPro.TextMeshProUGUI>();
    }
    private void SSPlayer()
    {
        scorego = cc.score;
        m_score.text = cc.score.ToString();
    }
}
