using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
	[SerializeField]
	private float m_JumpSpeed = 10f;
	private bool m_onAir = true;
	private SpawnerController spawner;
	public GameObject cameraGameOver;
	public GameObject Hud;
	[SerializeField]
    public int score
    {
        get { return m_score; }
        private set { m_score = value; }
    }

	private bool death;
    public bool m_death
    {
        get { return death; }
        private set { death = value; }
    }
    private int m_score = 0;
	private bool m_escudo;
	public bool escudo
	{
		get { return m_escudo; }
		private set { m_escudo = value; }
	}
    private Animator m_animator;
	private Rigidbody2D m_rigidBody;

	private int count=1;
	private int frames = 0;

	public delegate void PlayerDeath();
	public event PlayerDeath OnPlayerDeath;

    public delegate void ScorePlayer();
    public event ScorePlayer ScorePlayerScreen;





    void Awake()
	{
		m_rigidBody = GetComponent<Rigidbody2D>();
		m_animator = GetComponent<Animator>();
	}
	
    // Start is called before the first frame update
    void Start()
    {
		
		Respawn();
	}

    // Update is called once per frame
    void Update()
    {
		if (death)
			return;

		frames++;
        if (Input.GetKeyDown(KeyCode.E))
        {
			m_escudo = true;
            m_animator.Play("Protect");

        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            m_escudo = false;

        }
        if (Input.GetKeyDown(KeyCode.S))
		{
			m_rigidBody.gravityScale = 4;
			if (!m_onAir)
            {
				GetComponent<BoxCollider2D>().size = new Vector2(0.22f, 0.17f);
				GetComponent<BoxCollider2D>().offset = new Vector2(0.009f, -0.09f);
				m_animator.Play("Deslizar");
			}
			
		}

		if (!m_onAir && 
			(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.W)))
		{
			m_rigidBody.velocity = new Vector3(m_rigidBody.velocity.x,m_JumpSpeed,0);
			m_animator.Play("Jump");
		}
		if (Input.GetKeyUp(KeyCode.S)){
			/*spriteRenderer.sprite = oldSprite; */
			GetComponent<BoxCollider2D>().size = new Vector2(0.17f, 0.25f);
			GetComponent<BoxCollider2D>().offset = new Vector2(-0.037f, -0.047f);
			m_rigidBody.gravityScale = 1;
		}

		if (frames==(60*count))
		{
            m_score++;
			
			count++;
			ScorePlayerScreen.Invoke();

        }
		if (m_score >= 100)
		{
			m_JumpSpeed = 6;

        }
    }

	void OnCollisionEnter2D(Collision2D col)
    {
		if(col.gameObject.tag == "Terra")
			m_onAir = false;
    }

	void OnCollisionExit2D(Collision2D col)
    {
		if(col.gameObject.tag == "Terra")
			m_onAir = true;
    }
	void OnTriggerEnter2D(Collider2D col)
    {
		if (col.gameObject.tag == "PichoPinchito")
        {
            /*this.death = true;
            cameraGameOver.SetActive(true);
            Hud.SetActive(false);*/
            Death();
        }
		if (col.gameObject.tag == "Bala")
        {
			if (this.escudo == true)
			{
				Destroy(col.gameObject);
			}
			else
			{
                Death();
            }
        }

    }
	void Death()
	{
		OnPlayerDeath.Invoke();
		death = true;

    }

	public void Respawn()
	{
        score = 0;
        escudo = false;
		death = false;
        GetComponent<BoxCollider2D>().size = new Vector2(0.17f, 0.25f);
        GetComponent<BoxCollider2D>().offset = new Vector2(-0.037f, -0.047f);
        m_onAir = false;
        m_rigidBody.gravityScale = 1;
    }

}
