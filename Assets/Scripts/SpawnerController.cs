using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    [SerializeField]
    CharacterController cc;
    [SerializeField] 
    GameObject GameOver;
    [SerializeField]
    private GameObject[] m_ElementASpawnejar;
    [SerializeField]
    private float m_SpawnRate = 3f;
    private float m_SpawnRateDelta = 3f;
    private float positiony;
    private float velocityx;
    private bool dificultat;
   
    void Awake()
    {
        m_SpawnRateDelta = m_SpawnRate;
    }
    void Start()
    {
        cc.OnPlayerDeath += PlayerDead;
        Retry();
    }

    private void PlayerDead()
    {
        StopAllCoroutines();
        GameOver.SetActive(true);
    }

    private void Restart()
    {

    }


    void ClickPlay()
    {
        m_SpawnRate = 3f;
        m_SpawnRateDelta = 3f;
        Destroy(this.gameObject);
    }
    IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            int nRand = Random.Range(0, m_ElementASpawnejar.Length);
            if (nRand == 2 || nRand == 4)
            {
                if (dificultat == true)
                {
                    if (nRand == 2)
                    {
                        velocityx = -12;
                        positiony = 2f;
                        
                    }
                    else
                    {
                        velocityx = -10;
                        positiony = -0.14f;
                    }
                }
                else
                {
                    velocityx = -10;
                    positiony = -0.14f;
                }
                

            }
            else if (nRand == 1 || nRand == 3)
            {
                velocityx = -7;
                positiony = -1.16f;
            }
            else
            {
                velocityx = -5.15f;
                positiony = -1.28f;
            }
            GameObject spawned = Instantiate(m_ElementASpawnejar[nRand]);
            spawned.transform.position = new Vector3(transform.position.x, positiony, transform.position.z);
            spawned.GetComponent<Rigidbody2D>().velocity = new Vector3(velocityx, 0, 0);
            yield return new WaitForSeconds(m_SpawnRate);
        }
    }

    IEnumerator IncreaseDifficulty()
    {
        while (true)
        {
            yield return new WaitForSeconds(25);
            m_SpawnRate -= 0.5f;

            if (m_SpawnRate <= 0.5f)
            {
                m_SpawnRate = 0.5f;
            }
                
            if (m_SpawnRate == 1.5f)
            {
                dificultat = true;
            }

            else
            {
                dificultat = false;
            }
                
        }

    }

    public void Retry()
    {
        m_SpawnRate = 3f;
        m_SpawnRateDelta = 3f;
        dificultat = false;
        StartCoroutine(SpawnCoroutine());
        StartCoroutine(IncreaseDifficulty());
        cc.Respawn();
        GameOver.SetActive(false);
    }

}
